﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
// This is included here because it is forward declared in
// VadSim.h
#include "ui_mainwindow.h"

#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL)


#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>
#include <vtkMarchingCubes.h>
#include <vtkVoxelModeller.h>
#include <vtkSphereSource.h>
#include <vtkImageData.h>
#include <vtkDICOMImageReader.h>

#include <vtkPiecewiseFunction.h>
#include <vtkVolumeProperty.h>
#include <vtkVolume.h>
#include <vtkColorTransferFunction.h>
#include <vtkSTLReader.h>
#include <vtkArrowSource.h>
#include <vtkmath.h>
#include <vtkProperty.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <time.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkInteractorStyle.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <QFileDialog.h>
#include <QMessageBox>
#include <QDateTime.h>
#include <vtkAxesActor.h>
#include <vtkAxesActor.h>
#include <vtkBorderWidget.h>
#include <vtkTextWidget.h>
#include <vtkTextActor.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkTextRepresentation.h>
#include <vtkCoordinate.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCommand.h>
#include <vtkPointPicker.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSelection.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkDistanceWidget.h>
#include <vtkDistanceRepresentation.h>
#include <vtkCamera.h>
#include <vtkDistanceWidget.h>
#include <vtkDistanceRepresentation.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkAngleWidget.h>
#include <vtkCellPicker.h>
#include <vtkInteractorStyleTrackballActor.h>
#include "ActorPicker.h"
#include "vtkInteractorStyleSwitch.h"
#include "vtkCallbackCommand.h"
#include "vtkCommand.h"
#include "vtkInteractorStyleJoystickActor.h"
#include "vtkInteractorStyleJoystickCamera.h"
#include "vtkInteractorStyleTrackballActor.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkObjectFactory.h"
#include "vtkRenderWindowInteractor.h"
#include <vtkSmartPointer.h>
#include <vtkRendererCollection.h>
#include <vtkWorldPointPicker.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkObjectFactory.h>
#include <vtkDistanceRepresentation3D.h>
#include <vtkAngleRepresentation3D.h>
#include <vtkImageThreshold.h>
//#include <vtkImageMapper3D.h>
#include <vtkImageActor.h>
#include <vtkImageCast.h>
#include <vtkExecutionTimer.h>
#include <vsTreeItem.h>

#include "vsArtificialHeart.h"
#include "mycounter.h"

#include "vsvector.h"

#pragma comment(lib, "winmm.lib")




#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include  "vsextraction.h"
#include  "vsViewPort.h"


std::list< vsTreeItem* > m_treeItems;


void findItem(std::list< vsTreeItem* >& list,std::list< vsTreeItem* >::iterator& itr,vtkProp* prop ) {



    for( itr = list.begin(); itr != list.end(); ++itr ) {

        if( (*itr)->m_prop == prop ) {
            break;
        }

    }


}



void unselectAllItems(std::list< vsTreeItem* >& list ) {


    std::list< vsTreeItem* >::iterator itr;
    for( itr = list.begin(); itr != list.end(); ++itr ) {

        (*itr)->setSelected(false);

    }

}


// Define interaction style
class myInteractorStyleMove : public vtkInteractorStyleTrackballActor
{
private:
    Ui_VadSim *ui;
    //vtkActor* actSTL;
    vtkProp* actCT;
    vsViewPort vp[4];
public:
     static myInteractorStyleMove* New();
     vtkTypeMacro(myInteractorStyleMove, vtkInteractorStyleTrackballActor)

     myInteractorStyleMove():ui(0) {}//,actSTL(0)

     virtual ~myInteractorStyleMove() {}

/*
    void setSTL(vtkActor* stl) {

         printf("myInteractorStyleMove::setSTL\n");
         actSTL = stl;

   }
   */

    void setCT(vtkProp* ct) {

         printf("myInteractorStyleMove::setCT");
         actCT = ct;

   }

     void setUI(Ui_VadSim *_ui) {
        printf("ui_ok\n");
        ui = _ui;

    }

     void setSelectedObjectName(vtkProp* actor) {


         //QList<QString>* list = new QList<QString>();
         std::list<vsTreeItem*>::iterator itr = m_treeItems.begin();

         for( ; itr != m_treeItems.end(); ++itr ) {

             if( (*itr)->m_prop == actor  ) {

                 QString label = (*itr)->text(0);
                 qDebug("%s\n", qPrintable( label ) );

                 ui->label_objectName->setText(label.toAscii());
                 break;
             }

         }
     }


     void setVsViewport(vsViewPort _vp[4]) {

         memcpy(vp,_vp,sizeof(vp));

     }


     void OnWheelForward() {

         int xy[2];
         this->GetInteractor()->GetEventPosition(xy);

         //int* wh = ui->qvtkWidget->GetRenderWindow()->GetSize();

         //vp[0].isMouseOver(xy[0],xy[1],wh[0],wh[1]);

     }



    virtual void OnMiddleButtonDown()
    {

        // Forward events

        int x = this->Interactor->GetEventPosition()[0];
        int y = this->Interactor->GetEventPosition()[1];
        this->FindPokedRenderer(x, y);
        this->FindPickedActor(x, y);

        if (this->CurrentRenderer == NULL || this->InteractionProp == NULL)
          {
          std::cout << "Nothing selected." << std::endl;
          return;
          }

        vtkSmartPointer<vtkPropCollection> actors =
          vtkSmartPointer<vtkPropCollection>::New();

        this->InteractionProp->GetActors(actors);
        actors->InitTraversal();
        vtkActor* actor = vtkActor::SafeDownCast(actors->GetNextProp());

        if( actor != NULL ) {

            //actor->PrintSelf(std::cout,vtkIndent());

            if(actor != actCT)
            {

                vtkInteractorStyleTrackballActor::OnMiddleButtonDown();
                setRadioButtonState(actor);
                setSTLPosition(actor);
                setSTLAngle(actor);
                setSelectedObjectName(actor);


            } else {

                double p[3];
                actor->GetPosition(p);
                vtkInteractorStyleTrackballActor::OnMiddleButtonUp();
                actor->SetPosition(p);

                printf("not actSTL\n");

            }

        } else {


            vtkInteractorStyleTrackballActor::OnMiddleButtonUp();

            this->InteractionProp = NULL;

        }

        ui->qvtkWidget->GetRenderWindow()->Render();
    }


     virtual void OnMiddleButtonUp()
     {

         // Forward events

         int x = this->Interactor->GetEventPosition()[0];
         int y = this->Interactor->GetEventPosition()[1];
         this->FindPokedRenderer(x, y);
         this->FindPickedActor(x, y);

         if (this->CurrentRenderer == NULL || this->InteractionProp == NULL)
           {
           std::cout << "Nothing selected." << std::endl;
           return;
           }

         vtkSmartPointer<vtkPropCollection> actors =
           vtkSmartPointer<vtkPropCollection>::New();

         this->InteractionProp->GetActors(actors);
         actors->InitTraversal();
         vtkActor* actor = vtkActor::SafeDownCast(actors->GetNextProp());

         if( actor != NULL ) {


             if(actor != actCT)
             {
                 //printf("   actSTL\n");
                 vtkInteractorStyleTrackballActor::OnMiddleButtonUp();
                 conveyTransform(actor);
                setRadioButtonState(actor);
                setSTLPosition(actor);
                setSTLAngle(actor);
                setSelectedObjectName(actor);

             } else {

                 double p[3];
                 actor->GetPosition(p);
                 vtkInteractorStyleTrackballActor::OnMiddleButtonUp();
                 actor->SetPosition(p);


                 printf("not actSTL\n");


             }


        } else {


             vtkInteractorStyleTrackballActor::OnMiddleButtonUp();

             this->InteractionProp = NULL;

         }

        ui->qvtkWidget->GetRenderWindow()->Render();
     }



     void conveyTransform(vtkProp* actor) {

         if( actor == NULL )return;

         //  qDebug("convey");
         std::list< vsTreeItem* >::iterator itr;
         findItem(m_treeItems,itr,actor);
         if( itr == m_treeItems.end() ) return;

         vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();

         transform->PreMultiply();
         transform->Identity();



         vtkSmartPointer<vtkMatrix4x4> mat = vtkSmartPointer<vtkMatrix4x4>::New();

         mat = actor->GetMatrix();


         convey(transform,mat,itr);

        ui->qvtkWidget->GetRenderWindow()->Render();

     }



     void convey(vtkTransform* transform,vtkMatrix4x4* mat,std::list< vsTreeItem* >::iterator& itr) {


         //qDebug("convey2");

         transform->Push();
         transform->PreMultiply();

         transform->Concatenate(mat);




         //qDebug("num:%d",(*itr)->childCount());
         for( int i = 0; i < (*itr)->childCount(); ++i ) {


             vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();
             transform->GetMatrix(matrix);

             vtkSmartPointer<vtkMatrix4x4> currentUserMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
             currentUserMatrix = reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->GetUserMatrix();

             vtkSmartPointer<vtkMatrix4x4> currentMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
             currentMatrix = reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->GetMatrix();

              if(currentUserMatrix) {

                  qDebug("user mat");
                  // vtkMatrix4x4::Multiply4x4( currentUserMatrix, currentUserMatrix, currentMatrix );

              } else {

                  currentUserMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
                  currentUserMatrix->DeepCopy(currentMatrix);

              }


              //vtkMatrix4x4::Multiply4x4( currentUserMatrix, currentUserMatrix, reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_parentInvMat );


             //vtkMatrix4x4::Multiply4x4(matrix,matrix,currentUserMatrix);

             vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();

             transform->PostMultiply();
             transform->Identity();

             transform->SetMatrix(matrix);

             double set_p[3];
             double set_a[3];

             double p[3];
             transform->GetPosition(p);

             double a[3];
             transform->GetOrientation(a);

             double p_c[3];
             reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->GetPosition(p_c);

             double a_c[3];
             reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->GetOrientation(a_c);


                 for(int j = 0; j < 3; ++j ) {

                    set_p[j] = p[j] - reinterpret_cast<vsTreeItem*>((*itr)->child(i))->pos[j];
                    set_a[j] = a[j] - reinterpret_cast<vsTreeItem*>((*itr)->child(i))->angle[j];

                 }

                 for(int j = 0; j < 3; ++j ) {

                    set_p[j] += p_c[j];
                    set_a[j] += a_c[j];

                 }

                 for(int j = 0; j < 3; ++j ) {

                   reinterpret_cast<vsTreeItem*>((*itr)->child(i))->pos[j] = p[j];
                   reinterpret_cast<vsTreeItem*>((*itr)->child(i))->angle[j] = a[j];

                 }
                 reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->SetPosition(set_p);
                 reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->SetOrientation(set_a);



             //reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->SetPosition(p[0],p[1],p[2]);
             //reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->SetUserMatrix(matrix);
             reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->ComputeMatrix();
             //reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_parentInvMat->DeepCopy( matrix );

             //vtkMatrix4x4::Invert( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_parentInvMat, reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_parentInvMat );



         }




         for( int i = 0; i < (*itr)->childCount(); ++i ) {


            vtkSmartPointer<vtkMatrix4x4> local = vtkSmartPointer<vtkMatrix4x4>::New();
            local = reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop )->GetMatrix();



            std::list< vsTreeItem* >::iterator childItr;
            findItem(m_treeItems,childItr,reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>((*itr)->child(i))->m_prop ));

            //transform->Concatenate(local);
            convey(transform,local,childItr);

         }

         transform->Pop();



     }







     void setRadioButtonState(vtkProp* actor) {

         if( actor == NULL || ui == NULL ) {

            return;

         }


         std::list< vsTreeItem* >::iterator itr;
         findItem(m_treeItems,itr,actor);
         if( itr == m_treeItems.end()) return;

         if( (*itr)->m_bHasRel ) ui->radioButton_rel->setEnabled(true);
         else  ui->radioButton_rel->setEnabled(false);

         if( (*itr)->m_coord_mode == VCM_ABSOLUTE ) ui->radioButton_abs->setChecked(true);
         else   ui->radioButton_rel->setChecked(true);

         unselectAllItems(m_treeItems);
         (*itr)->setSelected(true);

     }


     void setSTLPosition(vtkProp* actor) {

         vtkActor* actSTL = vtkActor::SafeDownCast(actor);
         if( actSTL == NULL || ui == NULL ) {

             return;

         }


         std::list< vsTreeItem* >::iterator itr;
         findItem(m_treeItems,itr,actor);
         if( itr == m_treeItems.end()) return;


         //vtkSmartPointer<vtkMatrix4x4> mat = vtkSmartPointer<vtkMatrix4x4>::New();
         //reinterpret_cast<vtkProp3D*>( (*itr)->m_prop)->GetMatrix(mat);

         double p[3];
         reinterpret_cast<vtkProp3D*>( (*itr)->m_prop)->GetPosition(p);



         if( ui->radioButton_rel->isChecked() ) {

             for(int j = 0; j < 3; ++j ) {

                p[j] = p[j] - (*itr)->pos[j];
                //set_a[j] = a[j] - reinterpret_cast<vsTreeItem*>((*itr)->child(i))->angle[j];

             }

         }


         //std::cout << "p: " << p[0] << " " << p[1] << " " << p[2] << std::endl;

         ui->edit_x->setText(QString::number(p[0]));
         ui->edit_y->setText(QString::number(p[1]));
         ui->edit_z->setText(QString::number(p[2]));



     }


     void setPosEdit() {

         QList<QTreeWidgetItem*> items = ui->treeWidget->selectedItems();

         double p[3];
         reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(items.at(0)) )->GetPosition(p);

         if( ui->radioButton_rel->isChecked() ) {

             for(int j = 0; j < 3; ++j ) {

                p[j] = p[j] - reinterpret_cast<vsTreeItem*>(items.at(0))->pos[j];


             }

         }


         ui->edit_x->setText(QString::number(p[0]));
         ui->edit_y->setText(QString::number(p[1]));
         ui->edit_z->setText(QString::number(p[2]));

     }



     void setSTLAngle(vtkProp* actor) {

         vtkActor* actSTL = vtkActor::SafeDownCast(actor);
        if( actSTL == NULL || ui == NULL ) {

            return;

        }



        std::list< vsTreeItem* >::iterator itr;
        findItem(m_treeItems,itr,actor);
        if( itr == m_treeItems.end()) return;

        vtkSmartPointer<vtkMatrix4x4> mat = vtkSmartPointer<vtkMatrix4x4>::New();
        actSTL->GetMatrix(mat);

        vtkSmartPointer<vtkTransform> trans = vtkSmartPointer<vtkTransform>::New();

        trans->SetMatrix(mat);

        double angle[3];
        trans->GetOrientation(angle);


        if( ui->radioButton_rel->isChecked() ) {

            for(int j = 0; j < 3; ++j ) {

              angle[j] = angle[j] - (*itr)->angle[j];

            }

        }

        ui->edit_angle_x->setText(QString::number(angle[0]));
        ui->edit_angle_y->setText(QString::number(angle[1]));
        ui->edit_angle_z->setText(QString::number(angle[2]));

     }




     virtual void OnLeftButtonDown()
     {
        vtkInteractorStyleTrackballActor::OnLeftButtonDown();
        vtkSmartPointer<vtkPropCollection> actors =
          vtkSmartPointer<vtkPropCollection>::New();


        qDebug("1");
        if( this->InteractionProp == NULL) return;

        qDebug("1-1");
        this->InteractionProp->GetActors(actors);
        if( actors != NULL ) {

            qDebug("2");
            actors->InitTraversal();
            vtkActor* actor = vtkActor::SafeDownCast(actors->GetNextProp());

            if( actor == NULL ) {
                return;
            }

            qDebug("3");
            setRadioButtonState(actor);
            setSTLPosition(actor);
            setSTLAngle(actor);
            setSelectedObjectName(actor);
            qDebug("4");

        }


        qDebug("2-1");
        this->InteractionProp->GetVolumes(actors);
        if ( actors != NULL ) {

            qDebug("2");
            actors->InitTraversal();
            vtkActor* actor = vtkActor::SafeDownCast(actors->GetNextProp());
            if( actor == NULL ) {

                return;

            }
            qDebug("3");
            setRadioButtonState(actor);
            setSTLPosition(actor);
            setSTLAngle(actor);
            setSelectedObjectName(actor);
            qDebug("4");

        }


     }

     virtual void OnLeftButtonUp()
     {
        vtkInteractorStyleTrackballActor::OnLeftButtonUp();
        vtkSmartPointer<vtkPropCollection> actors =
          vtkSmartPointer<vtkPropCollection>::New();

        if( this->InteractionProp == NULL) return;
        this->InteractionProp->GetActors(actors);

        if( actors == NULL ) {

            return;

        }

        actors->InitTraversal();
        vtkActor* actor = vtkActor::SafeDownCast(actors->GetNextProp());
        if( actor == NULL ) {

            return;

        }

        conveyTransform(actor);
        setRadioButtonState(actor);
        setSTLPosition(actor);
        setSTLAngle(actor);
        setSelectedObjectName(actor);

     }

};



vtkStandardNewMacro(myInteractorStyleMove);


// Define interaction style
class MouseInteractorStyle : public vtkInteractorStyleTrackballCamera
{
private:
    vtkDistanceRepresentation* distRep;
    vtkAngleRepresentation* angleRep;

  public:
    static MouseInteractorStyle* New();
    vtkTypeMacro(MouseInteractorStyle, vtkInteractorStyleTrackballCamera);

    MouseInteractorStyle():distRep(0),angleRep(0) {



    }





    void setDistRepresetation(vtkDistanceRepresentation *_rep) {

        distRep = _rep;

    }



    void setAngleRepresetation(vtkAngleRepresentation *_rep) {

        angleRep = _rep;

    }

};

vtkStandardNewMacro(MouseInteractorStyle);



class vtkInteractorStyleMySwitch:public vtkInteractorStyleSwitch {

private:

public:

        static vtkInteractorStyleMySwitch* New();
        vtkTypeMacro(vtkInteractorStyleMySwitch, vtkInteractorStyleSwitch)


        vtkInteractorStyleMySwitch::vtkInteractorStyleMySwitch()
        {

            printf("aaaaaaaaaaa\n");
          this->JoystickActor = vtkInteractorStyleJoystickActor::New();
          this->JoystickCamera = vtkInteractorStyleJoystickCamera::New();
          this->TrackballActor = myInteractorStyleMove::New();
          this->TrackballCamera = MouseInteractorStyle::New();
          this->JoystickOrTrackball = VTKIS_TRACKBALL;
          this->CameraOrActor = VTKIS_ACTOR;

            this->CurrentStyle = 0;



        }

        void setSTL(vtkActor* stl) {

            printf("vtkInteractorStyleMySwitch::setSTL\n");
            //reinterpret_cast<myInteractorStyleMove*>(this->TrackballActor)->setSTL(stl);

        }



        void setCT(vtkProp* stl) {


            reinterpret_cast<myInteractorStyleMove*>(this->TrackballActor)->setCT(stl);

        }


        void setUI(Ui_VadSim *_ui) {


            reinterpret_cast<myInteractorStyleMove*>(this->TrackballActor)->setUI(_ui);
        }



        void setDistRepresetation(vtkDistanceRepresentation *_rep) {

            reinterpret_cast<MouseInteractorStyle*>(this->TrackballCamera)->setDistRepresetation(_rep);

        }



        void setAngleRepresetation(vtkAngleRepresentation *_rep) {

            reinterpret_cast<MouseInteractorStyle*>(this->TrackballCamera)->setAngleRepresetation(_rep);

        }

        void conveyTransform(vtkProp* actor) {

            reinterpret_cast<myInteractorStyleMove*>(this->TrackballActor)->conveyTransform(actor);

        }

        myInteractorStyleMove* getInteractorMove() {

            return reinterpret_cast<myInteractorStyleMove*>(this->TrackballActor);

        }

    };

    vtkStandardNewMacro(vtkInteractorStyleMySwitch)



void VadSim::setOrientionIcon() {

    axes = vtkSmartPointer< vtkAxesActor >::New();

    // Set up the widget
    widget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();


    widget->SetOrientationMarker( axes );
    widget->SetOutlineColor( 0.5, 0.5, 0.5 );
    widget->SetViewport( 0.2, 0.6, 0.4, 0.8 );
    widget->SetInteractor(m_renderinteractor);
    widget->SetCurrentRenderer(m_renderer[0]);
    widget->SetEnabled(1);
    widget->InteractiveOn();
    widget->SetPickingManaged(false);



}



void VadSim::loadCompoundSTL() {


    QString stlFname = QFileDialog::getOpenFileName(
            this,
            QString::fromLocal8Bit("複合STLの情報を記録したXMLファイルを開く" ) ,
            ".",
            QString::fromLocal8Bit( "XML file (*.xml)" ) );


    if ( !stlFname.isEmpty() )
    {

        boost::shared_ptr<vsArtificialHeart> ah(new vsArtificialHeart);

        qDebug( "compound:%s\n", qPrintable( stlFname ) );
        vtkSmartPointer<vsArtificialHeartParser> parser = vtkSmartPointer<vsArtificialHeartParser>::New();
        parser->parseXML(stlFname,ah);


        qDebug("parseXML:ok\n");
        printf("b:%s\n",qPrintable(ah->firstChild->name));
        addCompoundSTL((QTreeWidgetItem*)NULL,ah->firstChild);


    }




}




void VadSim::addCompoundSTL(QTreeWidgetItem* parent,boost::shared_ptr<vsArtificialHeart>& ah) {


    QString stlFname(ah->name);
    vsTreeItem* item = new vsTreeItem(parent,QStringList(QString::fromAscii(QFileInfo(stlFname).fileName().toAscii())));


    if(parent) {

        parent->addChild(item);
        item->m_bHasRel = true;

    }else ui->treeWidget->insertTopLevelItem(0,item);

    qDebug( "%s\n", qPrintable( stlFname ) );


    MyCounter load("CompoundSTLTime.txt");
    load.begin();

    /*
    std::string cStr = stlFname.toLocal8Bit();
    int len = cStr.length()+1;
    char* chr = new char[len];
    memcpy(chr, cStr.c_str(), len);

    cStr = chr;

    delete[] chr;
    */


    //qDebug( "%s\n", cStr.c_str() );

    vtkSmartPointer<vtkSTLReader> reader =
      vtkSmartPointer<vtkSTLReader>::New();
    reader->SetFileName(stlFname.toLocal8Bit());
    reader->Update();



    // Visualize
    vtkSmartPointer<vtkPolyDataMapper> mapper =
      vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(reader->GetOutputPort());

    actor_STL = vtkSmartPointer<vtkActor>::New();
    actor_STL->SetMapper(mapper);
    qDebug( "AddActor\n");
    m_renderer[0]->AddActor(actor_STL);




    load.end();


    item->m_prop = actor_STL;
    m_treeItems.push_back(item);

    if( ah->firstChild ) {

        addCompoundSTL(item,ah->firstChild);

    }

    if( ah->nextSibling ) {

        addCompoundSTL(parent,ah->nextSibling);

    }
    m_renderer[0]->ResetCamera();
    this->ui->qvtkWidget->GetRenderWindow()->Render();
}





void VadSim::loadSTL() {
    QString stlFname = QFileDialog::getOpenFileName(
            this,
            QString::fromLocal8Bit("STL開く" ) ,
            ".",
            QString::fromLocal8Bit( "STLfile (*.stl)" ) );


    if ( !stlFname.isEmpty() )
    {


        //ツリービューにアイテムを追加
        vsTreeItem* item = new vsTreeItem((QTreeWidgetItem*)NULL,QStringList(QString(QFileInfo(stlFname).fileName().toLocal8Bit())));
        ui->treeWidget->insertTopLevelItem(0,item);

        //printf("not empty \n");
        qDebug( "%s\n", qPrintable( stlFname ) );

       // if( actor_STL.GetPointer() != 0 )m_renderer[0]->RemoveActor(actor_STL);

        MyCounter load("STLTime.txt");
        load.begin();

        vtkSmartPointer<vtkSTLReader> reader =
          vtkSmartPointer<vtkSTLReader>::New();
        reader->SetFileName(stlFname.toLocal8Bit());
        reader->Update();


        // Visualize
        vtkSmartPointer<vtkPolyDataMapper> mapper =
          vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInputConnection(reader->GetOutputPort());

        actor_STL = vtkSmartPointer<vtkActor>::New();
        actor_STL->SetMapper(mapper);
        qDebug( "AddActor\n");
        m_renderer[0]->AddActor(actor_STL);

        style->setSTL(actor_STL);


        load.end();


        item->m_prop = actor_STL;
         m_treeItems.push_back(item);

    } else {

        printf(" empty \n");


    }


    m_renderer[0]->ResetCamera();
    this->ui->qvtkWidget->GetRenderWindow()->Render();
}




void VadSim::loadCT() {

    //フォルダを開くダイアログ
    QString tgfile = QFileDialog::getExistingDirectory(this,QString::fromLocal8Bit("ダイアログタイトル"),QString::fromLocal8Bit("初期ディレクトリ"));



    if ( !tgfile.isEmpty() )
    {
        // ファイルに対する処理


        //ツリービューにアイテムを追加
        vsTreeItem* item = new vsTreeItem((QTreeWidgetItem*)NULL,QStringList(QString("Human Body")));
        ui->treeWidget->insertTopLevelItem(0,item);



        MyCounter load("CTTime.txt");
        load.begin();
        vtkSmartPointer<vtkDICOMImageReader> reader =
        vtkSmartPointer<vtkDICOMImageReader>::New();
        reader->SetDirectoryName(tgfile.toAscii());
        reader->Update();


       // QMessageBox msgBox;
       // msgBox.setText("a");
        //msgBox.setInformativeText("MC?");
       // msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
       // msgBox.setDefaultButton(QMessageBox::Yes);




        int ret = QMessageBox::Yes;// msgBox.exec();



        if( ret == QMessageBox::No ) {

            //レンダリング
            vtkSmartPointer<vtkImageData> volume =
              vtkSmartPointer<vtkImageData>::New();
            volume->DeepCopy(reader->GetOutput());


            const double isoValue_bone = 300;
            const double isoValue_skin = 20;
            const double isoValue_organ = 20;

            vtkSmartPointer<vtkImageThreshold> imageThreshold =
              vtkSmartPointer<vtkImageThreshold>::New();
            imageThreshold->SetInputDataObject(0,volume);
            unsigned char lower = -5000;
            unsigned char upper = 5;

            imageThreshold->ThresholdBetween(lower, upper);
            imageThreshold->ReplaceInOn();
            imageThreshold->SetInValue(0);
            imageThreshold->Update();

            vtkDataObject* oimage = imageThreshold->GetOutputDataObject(0);

           imageThreshold->SetInputDataObject(oimage);
           imageThreshold->ThresholdBetween(30, 5000);
           imageThreshold->ReplaceInOn();
           imageThreshold->SetInValue(0);
           imageThreshold->Update();

            vtkSmartPointer<vtkMarchingCubes> surface_bone =
                vtkSmartPointer<vtkMarchingCubes>::New();



            vtkSmartPointer<vtkMarchingCubes> surface_skin =
                vtkSmartPointer<vtkMarchingCubes>::New();


            vtkSmartPointer<vtkMarchingCubes> surface_organ =
                vtkSmartPointer<vtkMarchingCubes>::New();

    #if VTK_MAJOR_VERSION <= 5
            //surface_bone->SetInput(volume);
           // surface_skin->SetInput(volume);
            //surface_organ->SetInput(result);
    #else
            //surface_bone->SetInputData(volume);
           // surface_skin->SetInputData(volume);
            //surface_organ->SetInputData(result);
    #endif

            surface_organ->SetInputData(imageThreshold->GetOutput());

            //surface_bone->ComputeNormalsOn();
            //surface_bone->SetValue(0, isoValue_bone);

            //surface_skin->ComputeNormalsOn();
            //surface_skin->SetValue(0, isoValue_skin);

            surface_organ->ComputeNormalsOn();
            surface_organ->SetValue(0, isoValue_organ);


/*
            vtkSmartPointer<vtkPolyDataMapper> mapper_bone =
                vtkSmartPointer<vtkPolyDataMapper>::New();
            mapper_bone->SetInputConnection(surface_bone->GetOutputPort());
            mapper_bone->ScalarVisibilityOff();
            */
/*
            vtkSmartPointer<vtkPolyDataMapper> mapper_skin =
                vtkSmartPointer<vtkPolyDataMapper>::New();
            mapper_skin->SetInputConnection(surface_skin->GetOutputPort());
            mapper_skin->ScalarVisibilityOff();
*/


     /*       //actor
            actor_bone = vtkSmartPointer<vtkActor>::New();
            actor_bone->SetMapper(mapper_bone);
            actor_bone->GetProperty()->SetColor( 1, 1, 1 );
            actor_bone->GetProperty()->SetOpacity( 0.4 );
*/
/*
            actor_skin = vtkSmartPointer<vtkActor>::New();
            actor_skin->SetMapper(mapper_skin);
            actor_skin->GetProperty()->SetColor( 1, 0.547237, 0.319073 );
            actor_skin->GetProperty()->SetOpacity( 0.2 );
*/

            vtkSmartPointer<vtkPolyDataMapper> mapper_organ =
                vtkSmartPointer<vtkPolyDataMapper>::New();
            mapper_organ->SetInputConnection(surface_organ->GetOutputPort());
            mapper_organ->ScalarVisibilityOff();



            vtkSmartPointer<vtkActor> actor_organ =
                    vtkSmartPointer<vtkActor>::New();
            actor_organ->SetMapper(mapper_organ);
            actor_organ->GetProperty()->SetColor( 1, 1, 1 );
            actor_organ->GetProperty()->SetOpacity( 0.5);


           // m_renderer[0]->AddActor(actor_skin); //set to m_renderer
            //m_renderer[0]->AddActor(actor_bone); //set to m_renderer

            m_renderer[0]->AddActor(actor_organ); //set to m_renderer
            m_renderer[0]->ResetCamera();
            style->setCT(actor_organ);

            item->m_prop = actor_organ;
            m_treeItems.push_back(item);


        } else {

            vtkVolume* volume;

            vol.setReader(reader);
            vol.update(ui->horizontalSlider_min->value(),ui->horizontalSlider_max->value(),m_renderer[0]);
            volume = vol.getVolume();


            m_extraction.setDicomReader(reader);
            m_extraction.getTexturedPlane( 0, actor_dicom_plane );
            m_renderer[0]->AddActor(actor_dicom_plane);

            m_renderer[0]->ResetCamera();

            style->setCT(volume);

            item->m_prop = volume;
            m_treeItems.push_back(item);




        }
        load.end();

        ui->horizontalSlider->setMinimum(0);
        int* ext = reader->GetDataExtent();
        ui->horizontalSlider->setMaximum(ext[5]);

        flash();


    }





}



void VadSim::resetCamera( int i ) {

    qDebug("reset cam");
    vsVector eyepos[4];
    vsVector focuspos[4];
    vsVector updir[4];

        eyepos[0] = vsVector(0,5000,0);
        eyepos[1] = vsVector(0, 0,100);//sagittal?
        eyepos[2] = vsVector(100,   0,0);//coronal?
        eyepos[3] = vsVector(0,   100,0);//axitial?



        focuspos[0] = vsVector(0,0,0);
        focuspos[1] = vsVector(0,0,0);//sagittal?
        focuspos[2] = vsVector(0,0,0);//coronal?
        focuspos[3] = vsVector(0,0,0);//axitial?


        updir[0] = vsVector(0,0,-1);
        updir[1] = vsVector(1,0,0);//sagittal?
        updir[2] = vsVector(0,1,0);//coronal?
        updir[3] = vsVector(0,0,-1);//axitial?

        qDebug("reset cam:%d",i);
        if( m_camera[i].Get() == 0 ) {

            qDebug("new cam:%d",i);
            m_camera[i] = vtkSmartPointer<vtkCamera>::New();

        }


        qDebug("camera:%d",i);

        //視点を(x,y,z) = (0,5000,0)にセット
        m_camera[i]->SetPosition( eyepos[i].x, eyepos[i].y, eyepos[i].z );

        //注視点を(x,y,z) = (0,0,0)にセット
        m_camera[i]->SetFocalPoint( focuspos[i].x, focuspos[i].y, focuspos[i].z );

        //上方定義ベクトルを(x,y,z) = (0,0,-1)にセット
        m_camera[i]->SetViewUp( updir[i].x, updir[i].y, updir[i].z );








        m_camera[i]->SetParallelProjection(true);

        qDebug("set cam to renderer:%d",i);

        //レンダラーにカメラをセット
        m_renderer[i]->SetActiveCamera(m_camera[i]);

}


// Constructor
VadSim::VadSim(QWidget *parent):
    QMainWindow(parent)
{


    initGUI();
    initVTK();

    printf("起動に成功\n");


}


void VadSim::initGUI() {
    printf("> start initGUI\n");


    this->ui = new Ui::VadSim;
    this->ui->setupUi(this);

    // Set up action signals and slots
    connect(this->ui->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));
    connect(this->ui->button_dist, SIGNAL(clicked()), this, SLOT(slotSetDistanceWidget()));
    connect(this->ui->button_angle, SIGNAL(clicked()), this, SLOT(slotSetAngleWidget()));
    connect(this->ui->resetCamera, SIGNAL(clicked()), this, SLOT(slotResetCamera()));
    connect(this->ui->edit_x, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
    connect(this->ui->edit_y, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
    connect(this->ui->edit_z, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));

    connect(this->ui->edit_angle_x, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
    connect(this->ui->edit_angle_y, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));
    connect(this->ui->edit_angle_z, SIGNAL(editingFinished()), this, SLOT(slotEditingFinished()));

    connect(this->ui->radioButton_abs, SIGNAL(clicked()), this, SLOT(slotRadioClicked()));
    connect(this->ui->radioButton_rel, SIGNAL(clicked()), this, SLOT(slotRadioClicked()));

    connect(this->ui->radioButton_rel, SIGNAL(clicked()), this, SLOT(slotRadioClicked()));
qDebug("init: radio");
    connect(this->ui->treeWidget,SIGNAL(itemClicked(QTreeWidgetItem*,int)),this,SLOT(slotTreeSelected(QTreeWidgetItem*, int)));
    connect(this->ui->horizontalSlider,SIGNAL(valueChanged(int)),this,SLOT(slotSlideBarChanged(int)));
    connect(this->ui->horizontalSlider_min,SIGNAL(valueChanged(int)),this,SLOT(slotSlideBarChanged2(int)));
    connect(this->ui->horizontalSlider_max,SIGNAL(valueChanged(int)),this,SLOT(slotSlideBarChanged2(int)));
qDebug("init: slidebar");
    ui->treeWidget->setColumnCount(1);
    ui->treeWidget->setHeaderLabel (QString::fromLocal8Bit("アイテム一覧"));

    ui->radioButton_abs->setChecked(true);
    ui->radioButton_rel->setEnabled(false);


    //メニューの初期化
    initMenu();
    printf("> finish initMenu\n");
qDebug("init: ok");
}




void VadSim::initMenu() {

    printf("> start initMenu\n");
    openDicomAct = new QAction(QString::fromLocal8Bit("&OpenDICOM"), this);

    openDicomAct->setStatusTip(QString::fromLocal8Bit("OpenDICOM"));
    connect(openDicomAct, SIGNAL(activated()), this, SLOT(loadCT()));

    openSTLAct = new QAction(QString::fromLocal8Bit("&OpenSTL"),this);
    openSTLAct->setStatusTip(QString::fromLocal8Bit("OpenSTL"));
    connect(openSTLAct, SIGNAL(activated()), this, SLOT(loadSTL()));

    openCompoundSTLAct = new QAction(QString::fromLocal8Bit("&OpenSTL(Compound)"),this);
    openCompoundSTLAct->setStatusTip(QString::fromLocal8Bit("あらかじめパーツ分けされた複数のSTLをグループ化して読み込みます"));
    connect(openCompoundSTLAct, SIGNAL(activated()), this, SLOT(loadCompoundSTL()));

    fileMenu = this->menuBar()->addMenu(QString::fromLocal8Bit(("ファイル(&F)")));
    fileMenu->addAction(openDicomAct);
    fileMenu->addAction(openSTLAct);
    fileMenu->addAction(openCompoundSTLAct);
    //fileMenu->insertSeparator(openSTLAct); //区切りを入れる
    printf("> finish initMenu\n");
}



#include "vsViewport.h"
#define VP_NUM 1
void VadSim::initVTK() {



    vsViewPort vp[4];

    /*
    vp[0].p1_x = 0.0;
    vp[0].p1_y = 0.5;

    vp[0].p2_x = 0.5;
    vp[0].p2_y = 1.0;
    vp[0].r = 0.0980;
    vp[0].g = 0.0980;
    vp[0].b = 0.4392;
*/
    vp[0].p1_x = 0.0;
    vp[0].p1_y = 0.0;

    vp[0].p2_x = 1.0;
    vp[0].p2_y = 1.0;
    vp[0].r = 0.0980;
    vp[0].g = 0.0980;
    vp[0].b = 0.4392;


    vp[1].p1_x = 0.5;
    vp[1].p1_y = 0.5;

    vp[1].p2_x = 1.0;
    vp[1].p2_y = 1.0;
    vp[1].r = vp[1].g = vp[1].b = 0.7;



    vp[2].p1_x = 0.0;
    vp[2].p1_y = 0.0;

    vp[2].p2_x = 0.5;
    vp[2].p2_y = 0.5;
    vp[1].r = vp[2].g = vp[2].b = 0.5;


    vp[3].p1_x = 0.5;
    vp[3].p1_y = 0.0;

    vp[3].p2_x = 1.0;
    vp[3].p2_y = 0.5;
    vp[3].r = vp[3].g = vp[3].b = 0.3;

    //レンダラーの作成
    for(int i = 0; i < VP_NUM; ++i ) {

        m_renderer[i] =  vtkSmartPointer<vtkRenderer>::New();


        //背景色を(r,g,b) = (0.0980,0.0980,0.4392)
        m_renderer[i]->SetBackground( vp[i].r,vp[i].g,vp[i].b );
        m_renderer[i]->SetViewport(vp[i].p1_x,vp[i].p1_y,vp[i].p2_x,vp[i].p2_y );

        resetCamera(i);

        //Windowにレンダラーをセット
        this->ui->qvtkWidget->GetRenderWindow()->AddRenderer(m_renderer[i]);

    }




    //現在のインターアクター(イベントハンドラ)を取得
    m_renderinteractor = this->ui->qvtkWidget->GetRenderWindow()->GetInteractor();

    //方向を示すアイコンのセット
    setOrientionIcon();




    //カメラモードと，移動モードの選択ができるインターアクションスタイルの設定
    style = vtkSmartPointer<vtkInteractorStyleMySwitch>::New();
    style->setUI(this->ui);

    m_renderinteractor->SetInteractorStyle( style );


    style->SetDefaultRenderer(m_renderer[0]);

    //内部の選択判定に使うのでSTLのIDを取得
    //style->Stl = actor_STL.Get();

    //スタイルをインターアクターにセット
    //m_renderinteractor->SetInteractorStyle( style );



}

void VadSim::flash() {

    for( int i = 0; i < VP_NUM; ++i ) {

        m_renderer[i]->Render();

    }

    this->ui->qvtkWidget->GetRenderWindow()->Render();
}

void VadSim::slotSlideBarChanged2(int val) {

    vtkVolume* volume;
    vol.update(ui->horizontalSlider_min->value(),ui->horizontalSlider_max->value(),m_renderer[0]);
    volume = vol.getVolume();
    style->setCT(volume);
    //item->m_prop = volume;

    ui->ct_min->setText(QString::number(ui->horizontalSlider_min->value()));
    ui->ct_max->setText(QString::number(ui->horizontalSlider_max->value()));


    flash();

}

void VadSim::slotSlideBarChanged(int val) {


    m_renderer[0]->RemoveActor(actor_dicom_plane);
    m_extraction.getTexturedPlane(val,actor_dicom_plane);
    m_renderer[0]->AddActor(actor_dicom_plane);
    m_renderer[0]->Render();
    this->ui->qvtkWidget->GetRenderWindow()->Render();


}


void VadSim::slotTreeSelected(QTreeWidgetItem * item, int column ) {

    style->getInteractorMove()->setRadioButtonState( reinterpret_cast<vsTreeItem*>(item) ->m_prop);
    style->getInteractorMove()->setSTLPosition( reinterpret_cast<vsTreeItem*>(item) ->m_prop);
    style->getInteractorMove()->setSTLAngle( reinterpret_cast<vsTreeItem*>(item) ->m_prop);
    style->getInteractorMove()->setSelectedObjectName( reinterpret_cast<vsTreeItem*>(item) ->m_prop);

}



/**
 * @brief VadSim::setDistanceWidget 距離の測定を行うウィジェットの設定を行う
 */
void VadSim::slotSetDistanceWidget() {

    if(angleWidget)angleWidget->Off();
    distanceWidget = vtkSmartPointer<vtkDistanceWidget>::New();

    dist_representation = vtkSmartPointer<vtkDistanceRepresentation3D>::New();
    distanceWidget->SetInteractor(m_renderinteractor);
    distanceWidget->SetRepresentation(dist_representation);
    reinterpret_cast<vtkDistanceRepresentation *>(distanceWidget->GetRepresentation())->SetLabelFormat("%-#7.8g mm");
    distanceWidget->On();




}



void VadSim::slotRadioClicked() {


    QList<QTreeWidgetItem*> items = ui->treeWidget->selectedItems();



    if( ui->radioButton_abs->isChecked() ) {

         reinterpret_cast<vsTreeItem*>(items.at(0))->m_coord_mode = VCM_ABSOLUTE;

    } else {

         reinterpret_cast<vsTreeItem*>(items.at(0))->m_coord_mode = VCM_RELATIVE;

    }

    style->getInteractorMove()->setSTLPosition( reinterpret_cast<vsTreeItem*>(items.at(0)) ->m_prop);
    style->getInteractorMove()->setSTLAngle( reinterpret_cast<vsTreeItem*>(items.at(0)) ->m_prop);
     //style->getInteractorMove()->setPosEdit();

}


void VadSim::slotEditingFinished() {

   if(ui->treeWidget->size() == 0 )return;
    QList<QTreeWidgetItem*> items = ui->treeWidget->selectedItems();

    double pos[3];
    pos[0] = ui->edit_x->text().toDouble();
    pos[1] = ui->edit_y->text().toDouble();
    pos[2] = ui->edit_z->text().toDouble();

    double angle[3];
    angle[0] = ui->edit_angle_x->text().toDouble();
    angle[1] = ui->edit_angle_y->text().toDouble();
    angle[2] = ui->edit_angle_z->text().toDouble();

    if( ui->radioButton_rel->isChecked() ) {

        for( int i = 0; i < 3; ++i ) {

            pos[i] += reinterpret_cast<vsTreeItem*>(items.at(0))->pos[i];
            angle[i] += reinterpret_cast<vsTreeItem*>(items.at(0))->angle[i];

        }

    }


    reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(items.at(0))->m_prop)->SetPosition( pos[0], pos[1],pos[2] );
    reinterpret_cast<vtkProp3D*>( reinterpret_cast<vsTreeItem*>(items.at(0))->m_prop)->SetOrientation( angle[0], angle[1],angle[2] );
    style->conveyTransform(reinterpret_cast<vsTreeItem*>(items.at(0))->m_prop);

    this->ui->qvtkWidget->GetRenderWindow()->Render();

}


void VadSim::slotSetAngleWidget() {

    if(distanceWidget)distanceWidget->Off();
    angleWidget = vtkSmartPointer<vtkAngleWidget>::New();
    angleWidget->SetInteractor(m_renderinteractor);
    angle_representation = vtkSmartPointer<vtkAngleRepresentation3D>::New();
    angleWidget->SetInteractor(m_renderinteractor);
    angleWidget->SetRepresentation(angle_representation);
    angleWidget->On();

}


void VadSim::slotResetCamera() {
    printf("resetCam\n");

    qDebug("slotReset");


    qDebug("Reset is terminated.");
    for( int i = 0; i < VP_NUM; ++i ) {

        resetCamera(i);
        m_renderer[i]->ResetCamera();
        m_renderer[i]->Render();

    }

    this->ui->qvtkWidget->GetRenderWindow()->Render();

}


void VadSim::slotExit()
{
  qApp->exit();
}
