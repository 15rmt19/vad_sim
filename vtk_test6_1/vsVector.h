#ifndef VSVECTOR
#define VSVECTOR

    struct vsVector {
        double x,y,z;
        vsVector( double  _x, double _y, double _z ):x(_x),y(_y),z(_z) {
        }

        vsVector():x(0),y(0),z(0) {
        }
    };

#endif // VSVECTOR

