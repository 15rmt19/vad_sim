#include "vsviewport.h"


bool vsViewPort::isMouseOver( int x, int y, int ww, int wh ) {

    double vx_min = ww * p1_x;
    double vy_min = ww * p1_y;

    double vx_max = ww * p2_x;
    double vy_max = ww * p2_y;

    if( vx_min <= x && x <= vx_max  && vy_min <= y && y <= vy_max) {

        //ビューポート内にカーソルが存在
        return true;

    }

    //ビューポート内にカーソルは存在しない
    return false;

}



