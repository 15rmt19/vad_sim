#include "vsVolumeManager.h"

#include "vtkPiecewiseFunction.h"
#include "vtkVolumeProperty.h"
#include "vtkVolume.h"
#include "vtkColorTransferFunction.h"
#include "vtkImageThreshold.h"
#include "vtkDICOMImageReader.h"
#include "vtkRenderer.h"
#include <QDebug>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkGPUVolumeRayCastMapper.h>

vsVolumeManager::vsVolumeManager()
{
}


void vsVolumeManager::update( int ct_min, int ct_max, vtkRenderer* renderer ) {

    if( m_reader == 0) {
        qDebug("error! (vsVolumeManager::update)vtkReader is Null.");
        return;
    }

    if( m_volume != 0 ) {
        renderer->RemoveVolume(m_volume.GetPointer());
    }

    vtkSmartPointer<vtkImageThreshold> imageThreshold = vtkSmartPointer<vtkImageThreshold>::New();
    imageThreshold->SetInputConnection(m_reader->GetOutputPort());


    imageThreshold->ThresholdBetween(ct_min, ct_max);
    qDebug("min max = (%d %d)",ct_min,ct_max);
    imageThreshold->ReplaceInOn();
    imageThreshold->SetInValue(0);
    imageThreshold->Update();

     //ボリュームレンダリング
     mapper = vtkSmartPointer<vtkFixedPointVolumeRayCastMapper>::New();
     opacityFunc = vtkSmartPointer<vtkPiecewiseFunction>::New();
     property = vtkSmartPointer<vtkVolumeProperty>::New();
     m_volume = vtkSmartPointer<vtkVolume>::New();
     colorFunc = vtkColorTransferFunction::New();

     qDebug("3\n");
     property->ShadeOff();
     property->SetInterpolationType(VTK_LINEAR_INTERPOLATION);
    // property->SetInterpolationTypeToLinear();


     qDebug("5\n");
         colorFunc->AddRGBSegment(0.0, 0.6, 0.6, 0.6, 255.0, 0.6, 0.6, 0.6 );

         opacityFunc->AddSegment(  0, 0.0, 10, 0.0 );
         opacityFunc->AddSegment( 10, 0.0, 1000, 0.7 );



     mapper->SetInputConnection( imageThreshold->GetOutputPort() );
 qDebug("3\n");
     property->SetScalarOpacity(opacityFunc); // composite first.
     property->SetColor( colorFunc );

 qDebug("4\n");
     // connect up the volume to the property and the mapper
     m_volume->SetProperty( property );
     m_volume->SetMapper( mapper );
     m_volume->SetPickable(false);



 qDebug("6\n");
     mapper->SetBlendModeToComposite();

     qDebug("7");


     renderer->AddVolume( m_volume.GetPointer() );
    qDebug("reconstruct %d-%d",ct_min,ct_max);
}

void vsVolumeManager::setCTMinMax(int *min, int *max) {

    *min = m_ct_min;
    *max = m_ct_max;

}

void vsVolumeManager::setReader( vtkSmartPointer<vtkDICOMImageReader>& reader ) {

    m_reader = reader;

}

vtkVolume* vsVolumeManager::getVolume(  ) {

    return m_volume.Get();

}
