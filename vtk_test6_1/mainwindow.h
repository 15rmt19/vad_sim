﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H



#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)

#include <QMainWindow>
#include <vtkSmartVolumeMapper.h>
#include <vtkSmartPointer.h>
#include <vtkCollection.h>
#include <QMenu.h>
#include <QMenuBar.h>
#include <boost/shared_ptr.hpp>
#include "vsExtraction.h"
#include "vsVolumeManager.h"
#include "vsvector.h"

// Forward Qt class declarations
class Ui_VadSim;

class vtkRenderWindow;
class vtkRenderer;
class QVTKInteractor;
class vtkRenderWindowInteractor;
class vtkTextWidget;
class vtkAxesActor;
class vtkOrientationMarkerWidget;
class vtkCamera;

class vtkActor;
class vtkDistanceWidget;
class vtkAngleWidget;
class vtkInteractorStyleMySwitch;
class vtkDistanceRepresentation3D;
class vtkAngleRepresentation3D;
class vsTreeItem;
class QTreeWidgetItem;
struct vsArtificialHeart;

class VadSim : public QMainWindow
{
  Q_OBJECT
public:

  // Constructor/Destructor
  explicit VadSim(QWidget *parent = 0);
  ~VadSim() {}
  void flash();

protected:
  void initMenu();
  void initGUI();
  void initVTK();

public:
  void resetCamera(int v);
public slots:

  virtual void slotExit();
  void loadSTL();
  void loadCompoundSTL();
  void loadCT();
  void slotSetAngleWidget();
  void slotSetDistanceWidget();
  void slotResetCamera();
  void slotEditingFinished();
  void slotRadioClicked();
  void slotTreeSelected (QTreeWidgetItem * item, int column );
  void slotSlideBarChanged(int val);
  void slotSlideBarChanged2(int val);

private:
  void setOrientionIcon();
  void addCompoundSTL(QTreeWidgetItem* parent,boost::shared_ptr<vsArtificialHeart>& ah);
  // Designer form
  Ui_VadSim *ui;

  QMenu *fileMenu;
  QMenu *editMenu;
  QAction *openDicomAct;
  QAction *openSTLAct;
  QAction *openCompoundSTLAct;

  vsVolumeManager vol;

  vtkSmartPointer<vtkRenderer> m_renderer[4];
  vtkRenderWindowInteractor* m_renderinteractor;
  vtkSmartPointer<vtkTextWidget> textWidget ;
  vtkSmartPointer< vtkAxesActor > axes;
  vtkSmartPointer<vtkOrientationMarkerWidget>  widget;
  vtkSmartPointer<vtkCamera> m_camera[4];
  vtkSmartPointer<vtkActor> actor_STL;
  vtkSmartPointer<vtkActor> actor_bone;
  vtkSmartPointer<vtkActor> actor_skin;
  vtkSmartPointer<vtkActor> actor_dicom_plane;

  vtkSmartPointer<vtkDistanceWidget> distanceWidget;
  vtkSmartPointer<vtkAngleWidget> angleWidget;
  vtkSmartPointer<vtkInteractorStyleMySwitch> style;
  vtkSmartPointer<vtkDistanceRepresentation3D> dist_representation;
  vtkSmartPointer<vtkAngleRepresentation3D> angle_representation;

  vsExtraction m_extraction;

};
  extern std::list< vsTreeItem* > m_treeItems;
#endif // MAINWINDOW_H
