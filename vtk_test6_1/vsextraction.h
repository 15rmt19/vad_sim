#ifndef VSEXTRACTION_H
#define VSEXTRACTION_H
#include <vtkSmartPointer.h>

class vtkDICOMImageReader;
class vtkActor;


class vsExtraction
{
private:
    vtkSmartPointer<vtkDICOMImageReader> m_reader;
public:
    vsExtraction();
    void setDicomReader(vtkSmartPointer<vtkDICOMImageReader>& reader);
    void getTexturedPlane( int sid,vtkSmartPointer<vtkActor>& texturedPlane);
    void getActor();
};

#endif // VSEXTRACTION_H
