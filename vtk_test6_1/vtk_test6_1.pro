#-------------------------------------------------
#
# Project created by QtCreator 2014-12-17T14:49:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vtk_test6_1
TEMPLATE = app





CONFIG(debug,debug|release){

LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkCommonCore-6.1 -lvtksys-6.1 -lQVTKWidgetPlugin -lvtkViewsCore-6.1 -lvtkInteractionWidgets-6.1 -lvtkInfovisCore-6.1 -lvtkInfovisLayout-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkRenderingCore-6.1 -lvtkIOCore-6.1 -lvtkFiltersCore-6.1 -lvtklibxml2-6.1 -lvtkDICOMParser-6.1 -lvtkpng-6.1 -lvtkpng-6.1 -lvtktiff-6.1 -lvtkzlib-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkjpeg-6.1 -lvtkalglib-6.1 -lvtkexpat-6.1 -lvtkverdict-6.1 -lvtkmetaio-6.1 -lvtkNetCDF-6.1 -lvtksqlite-6.1 -lvtkexoIIc-6.1 -lvtkexpat-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkverdict-6.1 -lvtkmetaio-6.1 -lvtkNetCDF-6.1 -lvtksqlite-6.1 -lvtkexoIIc-6.1 -lvtkftgl-6.1 -lvtkfreetype-6.1 -lvtkImagingHybrid-6.1 -lvtkFiltersHybrid-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkInteractionStyle-6.1 -lvtkInteractionWidgets-6.1 -lvtkCommonSystem-6.1 -lvtkCommonMisc-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkgl2ps-6.1 -lvtkRenderingOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkgl2ps-6.1 -lQVTKWidgetPlugin -lvtkRenderingQt-6.1 -lvtkGUISupportQt-6.1 -lvtkViewsQt-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkFiltersSources-6.1 -lvtkfilterstexture-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkCommonExecutionModel-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkIOImage-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkRenderingImage-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkInteractionImage-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkCommonDataModel-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkRenderingVolume-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkRenderingVolumeOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkRenderingFreeTypeOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkGUISupportQtOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkRenderingOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkalglib-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkfreetype-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkRenderingFreeType-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkIOGeometry-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkFiltersGeometry-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkCommonComputationalGeometry-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkCommonTransforms-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkCommonMath-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkImagingMath-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkFiltersGeneral-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkInteractionWidgets-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkRenderingAnnotation-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkFiltersImaging-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkImagingColor-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/debug -lvtkImagingCore-6.1


}

CONFIG(release,debug|release){

LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkCommonCore-6.1 -lvtksys-6.1 -lQVTKWidgetPlugin -lvtkViewsCore-6.1 -lvtkInteractionWidgets-6.1 -lvtkInfovisCore-6.1 -lvtkInfovisLayout-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkRenderingCore-6.1 -lvtkIOCore-6.1 -lvtkFiltersCore-6.1 -lvtklibxml2-6.1 -lvtkDICOMParser-6.1 -lvtkpng-6.1 -lvtkpng-6.1 -lvtktiff-6.1 -lvtkzlib-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkjpeg-6.1 -lvtkalglib-6.1 -lvtkexpat-6.1 -lvtkverdict-6.1 -lvtkmetaio-6.1 -lvtkNetCDF-6.1 -lvtksqlite-6.1 -lvtkexoIIc-6.1 -lvtkexpat-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkverdict-6.1 -lvtkmetaio-6.1 -lvtkNetCDF-6.1 -lvtksqlite-6.1 -lvtkexoIIc-6.1 -lvtkftgl-6.1 -lvtkfreetype-6.1 -lvtkImagingHybrid-6.1 -lvtkFiltersHybrid-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkInteractionStyle-6.1 -lvtkInteractionWidgets-6.1 -lvtkCommonSystem-6.1 -lvtkCommonMisc-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkgl2ps-6.1 -lvtkRenderingOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkgl2ps-6.1 -lQVTKWidgetPlugin -lvtkRenderingQt-6.1 -lvtkGUISupportQt-6.1 -lvtkViewsQt-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkFiltersSources-6.1 -lvtkfilterstexture-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkCommonExecutionModel-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkIOImage-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkRenderingImage-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkInteractionImage-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkCommonDataModel-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkRenderingVolume-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkRenderingVolumeOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkRenderingFreeTypeOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkGUISupportQtOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkRenderingOpenGL-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkalglib-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkfreetype-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkRenderingFreeType-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkIOGeometry-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkFiltersGeometry-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkCommonComputationalGeometry-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkCommonTransforms-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkCommonMath-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkImagingMath-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkFiltersGeneral-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkInteractionWidgets-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkRenderingAnnotation-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkFiltersImaging-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkImagingColor-6.1
LIBS += -LC:/VTK/x64_6.1.0/lib/release -lvtkImagingCore-6.1

}

SOURCES += main.cpp\
        mainwindow.cpp \
    vsArtificialHeart.cpp \
    vsXmlReader.cpp \
    vstreeitem.cpp \
    tinyxml2.cpp \
    vsextraction.cpp \
    vsdicomgrayscaller.cpp \
    vsVolumeManager.cpp \
    vsviewport.cpp

HEADERS  += mainwindow.h \
    vsArtificialHeart.h \
    vsXmlReader.h \
    vstreeitem.h \
    tinyxml2.h \
    MyCounter.h \
    vsextraction.h \
    vsdicomgrayscaller.h \
    vsVolumeManager.h \
    vsViewport.h \
    vsVector.h


FORMS    += mainwindow.ui

INCLUDEPATH += $$quote(C:/Users/hommaLab/Desktop/vp/boost_1_55_0)
INCLUDEPATH += $$quote(C:/Users/hommaLab/Documents/Visual Studio 2008/VTK-6.1.0)
INCLUDEPATH += $$quote(C:/Users/hommaLab/Documents/Visual Studio 2008/VTK-6.1.0/GUISupport/Qt)
INCLUDEPATH += $$quote($$PWD/../../Visual Studio 2008/VTK-6.1.0/Common)
INCLUDEPATH += $$quote($$PWD/../../Visual Studio 2008/VTK-6.1.0/Common/Core)
INCLUDEPATH += $$quote($$PWD/../../Visual Studio 2008/VTK-6.1.0/Rendering)
INCLUDEPATH += $$quote($$PWD/../../Visual Studio 2008/VTK-6.1.0/Rendering/Core)
INCLUDEPATH += $$quote($$PWD/../../Visual Studio 2008/VTK-6.1.0/Rendering/OpenGL)
INCLUDEPATH += $$quote($$PWD/../../Visual Studio 2008/VTK-6.1.0/Filtering)
INCLUDEPATH += $$quote($$PWD/../../Visual Studio 2008/VTK-6.1.0/Filters/Sources)
INCLUDEPATH += $$quote($$PWD/../../Visual Studio 2008/VTK-6.1.0/lib/GUISupport/Qt)
INCLUDEPATH += $$quote(C:/Users/hommaLab/Documents/Visual Studio 2008/VTK-6.1.0/lib/Interaction/Style)
INCLUDEPATH += $$quote(C:/Users/hommaLab/Documents/Visual Studio 2008/VTK-6.1.0/lib/Rendering/Core)
INCLUDEPATH += $$quote(C:/Users/hommaLab/Documents/Visual Studio 2008/VTK-6.1.0/include)

