#ifndef VSVOLUMEPRODUCTOR_H
#define VSVOLUMEPRODUCTOR_H

#include "vtkSmartPointer.h"

class vtkFixedPointVolumeRayCastMapper;
class vtkPiecewiseFunction;
class vtkVolumeProperty;
class vtkVolume;
class vtkColorTransferFunction;
class vtkDICOMImageReader;
class vtkRenderer;


class vsVolumeManager
{
private:
    int m_ct_min;
    int m_ct_max;
private:
    vtkSmartPointer<vtkFixedPointVolumeRayCastMapper > mapper;
    vtkSmartPointer<vtkPiecewiseFunction> opacityFunc;
    vtkSmartPointer<vtkVolumeProperty> property;
    vtkSmartPointer<vtkVolume> m_volume;
    vtkSmartPointer<vtkColorTransferFunction> colorFunc;
    vtkSmartPointer<vtkDICOMImageReader> m_reader;

public:
    vsVolumeManager();

    void update( int ct_min, int ct_max,  vtkRenderer* renderer );
    vtkVolume* getVolume();
    void setReader(vtkSmartPointer<vtkDICOMImageReader>& reader);
    void setCTMinMax(int *min, int *max);
};

#endif // VSVOLUMEPRODUCTOR_H
