#ifndef VSDICOMGRAYSCALLER_H
#define VSDICOMGRAYSCALLER_H
#include <vtkSmartPointer.h>

class vtkImageData;


class vsDicomGrayScaller
{
private:
    int m_iWindowCenter;
    int m_iWindowWidth;
public:
    vsDicomGrayScaller();
    void getPixels(vtkSmartPointer<vtkImageData> &dstImage, vtkImageData *srcImage );
    void setWCWL( const int window_center, const int window_level );

};

#endif // VSDICOMGRAYSCALLER_H
