#ifndef VSTREEITEM_H
#define VSTREEITEM_H

#include <QTreeWidgetItem>
#include <vtkMatrix4x4.h>
#include <vtkSmartPointer.h>
class vtkProp;

enum VS_COORDMODE {

    VCM_RELATIVE,
    VCM_ABSOLUTE

};

class vsTreeItem : public QTreeWidgetItem
{
    //Q_OBJECT
private:
public://時間がないのでパブリックでいく

    bool m_bHasRel;///<相対表示があるか
    VS_COORDMODE m_coord_mode;
public:

    explicit vsTreeItem(QTreeWidgetItem * parent, const QStringList & strings, int type = Type);
    explicit vsTreeItem(QObject *parent = 0);
    vtkProp* m_prop;

    double pos[3];
    double angle[3];

    vtkSmartPointer<vtkMatrix4x4> m_parentInvMat;

signals:

public slots:

};

#endif // VSTREEITEM_H
