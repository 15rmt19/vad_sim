#ifndef MYCOUNTER_H
#define MYCOUNTER_H
#include <windows.h>
#include <mmsystem.h>
#include <list>
#include <fstream>

using namespace std;
#define FPSCOUNTER_QUERYPER_COUNTER        1
#define FPSCOUNTER_TIMEGETTIME             2

class MyCounter
{
private:
    LARGE_INTEGER nFreq;	///<周波数
    LARGE_INTEGER nBefore;	///<開始時のカウンタ
    LARGE_INTEGER nAfter;	///<終了時のカウンタ
    DWORD dwTime;
    std::ofstream fout;
    bool m_finish; ///<計測終了したらtrueに
    QString datestr;
    QString timestr;

public:
    MyCounter(const char* fname):m_finish(false) {

        fout.open( fname, ios::app );
        //変数の初期化
        memset(&nFreq,   0x00, sizeof nFreq);
        memset(&nBefore, 0x00, sizeof nBefore);
        memset(&nAfter,  0x00, sizeof nAfter);
        dwTime = 0;


    }

    inline void begin() {

        //計測開始！
        QDateTime dateTime = QDateTime::currentDateTimeUtc();
        datestr = dateTime.date().toString("yyyy/MM/dd");
        timestr = dateTime.time().toString("hh:mm:ss");

        m_finish = false;
        QueryPerformanceFrequency(&nFreq);
        QueryPerformanceCounter(&nBefore);

    }

    inline void end() {

        //計測終了！
        QueryPerformanceCounter(&nAfter);
        m_finish = true;


    }

    virtual ~MyCounter() {

        if( m_finish ) {
            //出力する
            dwTime = (DWORD)((nAfter.QuadPart - nBefore.QuadPart) * 1000 / nFreq.QuadPart);
            fout<<"["<<datestr.toAscii().data() << " " << timestr.toAscii().data() <<"]"<<"time:"<<dwTime<<"[ms]"<<std::endl;
        }
    }
};






#endif // MYCOUNTER_H
