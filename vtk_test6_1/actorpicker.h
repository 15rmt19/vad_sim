#ifndef ACTORPICKER_H
#define ACTORPICKER_H
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkProp3D.h>
#include <vtkSmartPointer.h>
#include <vtkPropCollection.h>


class ActorPicker : public vtkInteractorStyleTrackballActor
{
public:
    vtkTypeMacro(ActorPicker, vtkInteractorStyleTrackballActor);

    ActorPicker();
    virtual ~ActorPicker();
    static ActorPicker* New() {

        return new ActorPicker;

    }
public:

  virtual void OnLeftButtonDown();

vtkActor* Stl;

};

#endif // ACTORPICKER_H
