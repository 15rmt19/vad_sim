#include "vstreeitem.h"

vsTreeItem::vsTreeItem(QTreeWidgetItem * parent, const QStringList & strings, int type) :
    QTreeWidgetItem(parent,strings,type),m_bHasRel(false),m_coord_mode(VCM_ABSOLUTE),m_parentInvMat()
{
    m_parentInvMat = vtkSmartPointer<vtkMatrix4x4>::New();
    m_parentInvMat->Identity();
    pos[0] = pos[1] = pos[2] = 0;
    angle[0] = angle[1] = angle[2] = 0;
}
